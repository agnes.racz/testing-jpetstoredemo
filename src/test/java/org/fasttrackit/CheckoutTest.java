package org.fasttrackit;

import com.codeborne.selenide.WebDriverRunner;
import io.qameta.allure.*;
import org.fasttrackit.config.Config;
import org.fasttrackit.dataprovider.itemdataprovider.FishItemDataProvider;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.closeWebDriver;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

@Feature("Functionality - Checkout")
public class CheckoutTest extends Config {
    CatalogPage catalogPage = new CatalogPage();
    Header header = new Header();
    CartPage cartPage = new CartPage();
    SignInPage signInPage = new SignInPage();

    @BeforeMethod
    public void setupSession() {
        catalogPage.openCatalogPage();
    }

    @AfterMethod
    @Step("Close Web Driver")
    public static void closeSession() {
        closeWebDriver();
    }

    @Test(dataProviderClass = FishItemDataProvider.class, dataProvider = "koiItem", description = "Check out button on Cart page opens the Sign in page, in case of guest")
    @Description("Check out button on Cart page opens the Sign in page, in case of guest")
    @Story("Verifying Checkout button functionality")
    @Severity(SeverityLevel.NORMAL)
    public void checkout_button_opens_sign_in_page_in_case_of_guest(ItemData itemData) {
        header.clickOnFishButton();
        catalogPage.clickOnKoiSubcategoryLink();
        CatalogItem catalogItem = new CatalogItem(itemData.getID());
        catalogItem.clickOnAddToCartButton();
        cartPage.clickOnCheckoutButton();
        assertTrue(signInPage.isUsernameFieldDisplayed(), "Username input field must be displayed");
        assertTrue(signInPage.isPasswordFieldDisplayed(), "Password input field must be displayed");
        assertTrue(signInPage.isLoginButtonDisplayed(), "Login button must be displayed");
    }

    @Test(dataProviderClass = FishItemDataProvider.class, dataProvider = "koiItem", description = "Check out button on cart page opens checkout order form, in case of logged in user")
    @Description("Check out button on Cart page opens checkout order form, in case of logged in user")
    @Story("Verifying Checkout button functionality")
    @Severity(SeverityLevel.CRITICAL)
    public void checkout_button_opens_checkout_order_form_page(ItemData itemData) {
        header.clickOnSignInLink();
        signInPage.typeInUsername("j2ee");
        signInPage.typeInPassword("j2ee");
        signInPage.clickOnLoginButton();
        header.clickOnFishButton();
        catalogPage.clickOnKoiSubcategoryLink();
        CatalogItem catalogItem = new CatalogItem(itemData.getID());
        catalogItem.clickOnAddToCartButton();
        cartPage.clickOnCheckoutButton();
        assertEquals(WebDriverRunner.getWebDriver().getCurrentUrl(), "https://petstore.octoperf.com/actions/Order.action?newOrderForm=", "The opened page must be: https://petstore.octoperf.com/actions/Order.action?newOrderForm=");
    }

    @Test(dataProviderClass = FishItemDataProvider.class, dataProvider = "koiItem", description = "User can login after clicking on check out button on cart page")
    @Description("User can login after clicking on checkout button on cart page")
    @Story("Verifying if user can login after clicking checkout button")
    @Severity(SeverityLevel.CRITICAL)
    public void user_can_login_after_clicking_on_checkout_button_on_cart_page(ItemData itemData) {
        header.clickOnFishButton();
        catalogPage.clickOnKoiSubcategoryLink();
        CatalogItem catalogItem = new CatalogItem(itemData.getID());
        catalogItem.clickOnAddToCartButton();
        cartPage.clickOnCheckoutButton();
        signInPage.typeInUsername("j2ee");
        signInPage.typeInPassword("j2ee");
        signInPage.clickOnLoginButton();
        assertTrue(header.isSignOutLinkDisplayed(), "Sign in link must change to Sign out");
    }

    @Test(dataProviderClass = FishItemDataProvider.class, dataProvider = "koiItem", description = "Cart page opens after user login from cart page checkout")
    @Description("Cart page opens after user login from cart page checkout")
    @Story("Verifying if cart page opens after user login from cart page checkout")
    @Severity(SeverityLevel.MINOR)
    public void cart_page_opens_after_user_login_from_cart_page(ItemData itemData) {
        header.clickOnFishButton();
        catalogPage.clickOnKoiSubcategoryLink();
        CatalogItem catalogItem = new CatalogItem(itemData.getID());
        catalogItem.clickOnAddToCartButton();
        cartPage.clickOnCheckoutButton();
        signInPage.typeInUsername("j2ee");
        signInPage.typeInPassword("j2ee");
        signInPage.clickOnLoginButton();
        assertEquals(WebDriverRunner.getWebDriver().getCurrentUrl(), "https://petstore.octoperf.com/actions/Cart.action?viewCart=", "Expected to be on the Cart page");
    }
}
