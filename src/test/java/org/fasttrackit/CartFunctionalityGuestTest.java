package org.fasttrackit;

import io.qameta.allure.*;
import org.fasttrackit.config.Config;
import org.fasttrackit.dataprovider.itemdataprovider.FishItemDataProvider;
import org.fasttrackit.dataprovider.itemdataprovider.DogItemDataProvider;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static com.codeborne.selenide.Selenide.closeWebDriver;
import static org.testng.Assert.assertEquals;

@Feature("Functionality - Cart - as guest")
public class CartFunctionalityGuestTest extends Config {
    CatalogPage catalogPage = new CatalogPage();
    Header header = new Header();
    CartPage cartPage = new CartPage();

    @BeforeMethod
    public void setupSession() {
        catalogPage.openCatalogPage();
    }

    @AfterMethod
    @Step("Close Web Driver")
    public static void closeSession() {
        closeWebDriver();
    }

    @Test(dataProviderClass = FishItemDataProvider.class, dataProvider = "angelfishItem", description = "Cart contains one item added by guest")
    @Description("Cart contains item added by guest, from Angelfish subcategory")
    @Story("Verifying Add to cart functionality")
    @Severity(SeverityLevel.BLOCKER)
    public void cart_contains_item_added_by_guest_from_Angelfish_category(ItemData itemData) {
        header.clickOnFishButton();
        catalogPage.clickOnAngelfishSubcategoryLink();
        CatalogItem catalogItem = new CatalogItem(itemData.getID());
        catalogItem.clickOnAddToCartButton();
        CartItem cartItem = new CartItem(itemData.getID());
        assertEquals(cartItem.getCartItemID(), itemData.getID(), "The cart must contain item with ID " + itemData.getID());
    }

    @Test(dataProviderClass = FishItemDataProvider.class, dataProvider = "koiItem", description = "Cart contains one item added by user")
    @Description("Cart contains item added by guest, from Koi subcategory")
    @Story("Verifying Add to cart functionality")
    @Severity(SeverityLevel.BLOCKER)
    public void cart_contains_item_added_by_guest_from_Koi_category(ItemData itemData) {
        header.clickOnFishButton();
        catalogPage.clickOnKoiSubcategoryLink();
        CatalogItem catalogItem = new CatalogItem(itemData.getID());
        catalogItem.clickOnAddToCartButton();
        CartItem cartItem = new CartItem(itemData.getID());
        assertEquals(cartItem.getCartItemID(), itemData.getID(), "The cart must contain item with ID " + itemData.getID());
    }

    @Test(dataProviderClass = DogItemDataProvider.class, dataProvider = "labradorItem", description = "Cart contains one item added by guest")
    @Description("Cart contains item added by guest, from Labrador subcategory")
    @Story("Verifying Add to cart functionality")
    @Severity(SeverityLevel.BLOCKER)
    public void cart_contains_item_added_by_guest_from_Labrador_category(ItemData itemData) {
        header.clickOnDogsButton();
        catalogPage.clickOnLabradorSubcategoryLink();
        CatalogItem catalogItem = new CatalogItem(itemData.getID());
        catalogItem.clickOnAddToCartButton();
        CartItem cartItem = new CartItem(itemData.getID());
        assertEquals(cartItem.getCartItemID(), itemData.getID(), "The cart must contain item with ID " + itemData.getID());
    }


    @Test(dataProviderClass = FishItemDataProvider.class, dataProvider = "tigersharkItem", description = "Cart contains one item added by guest")
    @Description("Cart contains item added by guest, from Tiger Shark subcategory")
    @Story("Verifying Add to cart functionality")
    @Severity(SeverityLevel.BLOCKER)
    public void cart_contains_Toothless_Tiger_Shark_item_added_by_guest(ItemData itemData) {
        header.clickOnFishButton();
        catalogPage.clickOnTigerSharkSubcategoryLink();
        CatalogItem catalogItem = new CatalogItem(itemData.getID());
        catalogItem.clickOnAddToCartButton();
        CartItem cartItem = new CartItem(itemData.getID());
        assertEquals(cartItem.getCartItemID(), itemData.getID(), "The cart must contain item with ID " + itemData.getID());
    }

    @Test(dataProviderClass = FishItemDataProvider.class, dataProvider = "angelfishItem", description = "Cart contains only one item added by guest")
    @Description("Cart contains only one item, when only one item is added by guest, from Angelfish subcategory")
    @Story("Verifying Add to cart functionality")
    @Severity(SeverityLevel.CRITICAL)
    public void cart_contains_only_one_item_when_only_one_item_is_added_by_guest_from_angelfish_subcat(ItemData itemData) {
        header.clickOnFishButton();
        catalogPage.clickOnAngelfishSubcategoryLink();
        CatalogItem catalogItem = new CatalogItem(itemData.getID());
        catalogItem.clickOnAddToCartButton();
        CartItem cartItem = new CartItem(itemData.getID());
        assertEquals(cartPage.getCartItemsNumber(), 1, "Cart must contain only one item");
        assertEquals(cartItem.getCartItemQuantity(), "1", "Item quantity must be 1");
    }

    @Test(dataProviderClass = FishItemDataProvider.class, dataProvider = "koiItem", description = "Cart contains only one item added by guest")
    @Description("Cart contains only item, when only one item is added by guest, from Koi subcategory")
    @Story("Verifying Add to cart functionality")
    @Severity(SeverityLevel.CRITICAL)
    public void cart_contains_only_one_item_when_only_one_item_is_added_by_guest_from_koi_subcat(ItemData itemData) {
        header.clickOnFishButton();
        catalogPage.clickOnKoiSubcategoryLink();
        CatalogItem catalogItem = new CatalogItem(itemData.getID());
        catalogItem.clickOnAddToCartButton();
        CartItem cartItem = new CartItem(itemData.getID());
        assertEquals(cartPage.getCartItemsNumber(), 1, "Cart must contain only one item");
        assertEquals(cartItem.getCartItemQuantity(), "1", "Item quantity must be 1");
    }

    @Test(dataProviderClass = FishItemDataProvider.class, dataProvider = "angelfishItems", description = "Cart contains two different items, of the same subcategory added by guest ")
    @Description("Cart contains two different items of the Angelfish subcategory, added bu guest")
    @Story("Verifying Add to cart functionality")
    @Severity(SeverityLevel.CRITICAL)
    public void guest_can_add_to_cart_two_items_of_the_Angelfish_subcategory(List<ItemData> itemData) {
        header.clickOnFishButton();
        catalogPage.clickOnAngelfishSubcategoryLink();
        CatalogItem catalogItem = new CatalogItem(itemData.get(0).getID());
        catalogItem.clickOnAddToCartButton();
        header.clickOnFishButton();
        catalogPage.clickOnAngelfishSubcategoryLink();
        catalogItem = new CatalogItem(itemData.get(1).getID());
        catalogItem.clickOnAddToCartButton();
        CartItem first = new CartItem(itemData.get(0).getID());
        CartItem second = new CartItem(itemData.get(1).getID());
        assertEquals(first.getCartItemID(), itemData.get(0).getID(), "Cart must contain item with ID " + itemData.get(0).getID());
        assertEquals(second.getCartItemID(), itemData.get(1).getID(), "Cart must contain item with ID " + itemData.get(1).getID());
    }

    @Test(dataProviderClass = DogItemDataProvider.class, dataProvider = "labradorItems", description = "Cart contains two different items, of the same subcategory added by guest ")
    @Description("Cart contains two different items of the Labrador subcategory, added bu guest")
    @Story("Verifying Add to cart functionality")
    @Severity(SeverityLevel.CRITICAL)
    public void guest_can_add_to_cart_two_items_of_the_Labrador_subcategory(List<ItemData> itemData) {
        header.clickOnDogsButton();
        catalogPage.clickOnLabradorSubcategoryLink();
        CatalogItem catalogItem = new CatalogItem(itemData.get(0).getID());
        catalogItem.clickOnAddToCartButton();
        header.clickOnDogsButton();
        catalogPage.clickOnLabradorSubcategoryLink();
        catalogItem = new CatalogItem(itemData.get(1).getID());
        catalogItem.clickOnAddToCartButton();
        CartItem first = new CartItem(itemData.get(0).getID());
        CartItem second = new CartItem(itemData.get(1).getID());
        assertEquals(first.getCartItemID(), itemData.get(0).getID(), "Cart must contain item with ID " + itemData.get(0).getID());
        assertEquals(second.getCartItemID(), itemData.get(1).getID(), "Cart must contain item with ID " + itemData.get(1).getID());
    }

    @Test(dataProviderClass = FishItemDataProvider.class, dataProvider = "angelfishItem", description = "Guest can remove item from cart")
    @Description("Guest can remove Angelfish subcategory item from cart")
    @Story("Verifying Remove from cart functionality")
    @Severity(SeverityLevel.CRITICAL)
    public void guest_can_remove_Angelfish_subcategory_item_from_cart(ItemData itemData) {
        header.clickOnFishButton();
        catalogPage.clickOnAngelfishSubcategoryLink();
        CatalogItem catalogItem = new CatalogItem(itemData.getID());
        catalogItem.clickOnAddToCartButton();
        CartItem cartItem = new CartItem(itemData.getID());
        cartItem.clickOnRemoveButton();
        assertEquals(cartPage.getEmptyCartMessage(), "Your cart is empty.", "Cart must be empty, empty cart message must be displayed");
    }

    @Test(dataProviderClass = DogItemDataProvider.class, dataProvider = "labradorItem", description = "Guest can remove item from cart")
    @Description("Guest can remove Labrador subcategory item from cart")
    @Story("Verifying Remove from cart functionality")
    @Severity(SeverityLevel.CRITICAL)
    public void guest_can_remove_Labrador_subcategory_item_from_cart(ItemData itemData) {
        header.clickOnDogsButton();
        catalogPage.clickOnLabradorSubcategoryLink();
        CatalogItem catalogItem = new CatalogItem(itemData.getID());
        catalogItem.clickOnAddToCartButton();
        CartItem cartItem = new CartItem(itemData.getID());
        cartItem.clickOnRemoveButton();
        assertEquals(cartPage.getEmptyCartMessage(), "Your cart is empty.", "Cart must be empty, empty cart message must be displayed");
    }

    @Test(dataProviderClass = FishItemDataProvider.class, dataProvider = "angelfishItem", description = "Subtotal is 0, when item is removed from cart")
    @Description("Subtotal is 0, when Angelfish subcategory item is removed from cart, after adding only one item to cart")
    @Story("Verifying Remove from cart functionality")
    @Severity(SeverityLevel.CRITICAL)
    public void sub_total_is_0_when_Angelfish_subcategory_item_is_removed_from_cart_containing_1_item(ItemData itemData) {
        header.clickOnFishButton();
        catalogPage.clickOnAngelfishSubcategoryLink();
        CatalogItem catalogItem = new CatalogItem(itemData.getID());
        catalogItem.clickOnAddToCartButton();
        CartItem cartItem = new CartItem(itemData.getID());
        cartItem.clickOnRemoveButton();
        assertEquals(cartPage.getSubTotal(), 0.00, "Cart must be empty, sub total must be 0.00");
    }

    @Test(dataProviderClass = DogItemDataProvider.class, dataProvider = "labradorItem", description = "Subtotal is 0, when item is removed from cart")
    @Description("Subtotal is 0, when Angelfish subcategory item is removed from cart, after adding only one item to cart")
    @Story("Verifying Remove from cart functionality")
    @Severity(SeverityLevel.CRITICAL)
    public void sub_total_is_0_when_Labrador_subcategory_item_is_removed_from_cart_containing_1_item(ItemData itemData) {
        header.clickOnDogsButton();
        catalogPage.clickOnLabradorSubcategoryLink();
        CatalogItem catalogItem = new CatalogItem(itemData.getID());
        catalogItem.clickOnAddToCartButton();
        CartItem cartItem = new CartItem(itemData.getID());
        cartItem.clickOnRemoveButton();
        assertEquals(cartPage.getSubTotal(), 0.00, "Cart must be empty, sub total must be 0.00");
    }

    @Test(description = "Guest can change the quantity of an item in cart")
    @Description("Guest can change the quantity of an item in cart")
    @Story("Verifying Quantity change functionality")
    @Severity(SeverityLevel.CRITICAL)
    public void guest_can_change_the_quantity_of_an_item_in_cart() {
        header.clickOnFishButton();
        catalogPage.clickOnAngelfishSubcategoryLink();
        CatalogItem catalogItem = new CatalogItem("EST-1");
        catalogItem.clickOnAddToCartButton();
        CartItem first = new CartItem("EST-1");
        first.changeQuantityInCart("3");
        cartPage.clickOnUpdateCartButton();
        assertEquals(first.getTotalCost(), 49.5, "Total cost must be 49.5 (16.5*3)");
        assertEquals(cartPage.getSubTotal(), 49.5, "Subtotal must be 49.5 (16.5*3)");
    }

    @Test(description = "Guest can add two items to cart from different categories")
    @Description("Guest can add two items to cart from different Fish categories")
    @Story("Verifying Add to cart functionality")
    @Severity(SeverityLevel.CRITICAL)
    public void guest_can_add_items_to_cart_from_different_Fish_categories() {
        header.clickOnFishButton();
        catalogPage.clickOnAngelfishSubcategoryLink();
        CatalogItem catalogItem = new CatalogItem("EST-1");
        catalogItem.clickOnAddToCartButton();
        header.clickOnFishButton();
        catalogPage.clickOnTigerSharkSubcategoryLink();
        catalogItem = new CatalogItem("EST-3");
        catalogItem.clickOnAddToCartButton();
        CartItem first = new CartItem("EST-1");
        CartItem second = new CartItem("EST-3");
        assertEquals(first.getCartItemID(), "EST-1", "Cart must contain item with ID EST-1, Large Angelfish");
        assertEquals(second.getCartItemID(), "EST-3", "Cart must contain item with ID EST-3, Toothless Tiger Shark");
    }
}
