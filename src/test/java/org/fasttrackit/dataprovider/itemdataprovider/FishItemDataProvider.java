package org.fasttrackit.dataprovider.itemdataprovider;

import org.fasttrackit.ItemData;
import org.testng.annotations.DataProvider;

import java.util.ArrayList;
import java.util.List;

public class FishItemDataProvider {
    static ItemData largeA = new ItemData("EST-1", "Large Angelfish");
    static ItemData smallA = new ItemData("EST-2", "Small Angelfish");
    static ItemData adultMG = new ItemData("EST-20", "Adult Male Goldfish");
    static ItemData adultFG = new ItemData("EST-21", "Adult Female Goldfish");
    static ItemData spottedKoi = new ItemData("EST-4", "Spotted Koi");
    static ItemData spotlessKoi = new ItemData("EST-5", "Spotless Koi");
    static ItemData toothlessT = new ItemData("EST-3", "Toothless Tiger Shark");


    @DataProvider(name = "angelfishItem")
    public Object[][] getAngelfishItem() {
        return new Object[][]{
                {largeA},
                {smallA},
        };
    }

    @DataProvider(name = "goldfishItem")
    public Object[][] getGoldfishItem() {
        return new Object[][]{
                {adultMG},
                {adultFG},
        };
    }

    @DataProvider(name = "koiItem")
    public Object[][] getKoiItem() {
        return new Object[][]{
                {spottedKoi},
                {spotlessKoi},
        };
    }

    @DataProvider(name = "tigersharkItem")
    public Object[][] getTigerSharkItem() {
        return new Object[][]{
                {toothlessT},
        };
    }

    @DataProvider(name = "angelfishItems")
    public Object[][] getAngelfishItems() {
        List<ItemData> items = new ArrayList<>();
        items.add(largeA);
        items.add(smallA);
        return new Object[][]{
                {items},
        };
    }
}
