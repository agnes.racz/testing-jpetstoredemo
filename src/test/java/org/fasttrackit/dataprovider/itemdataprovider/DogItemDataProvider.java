package org.fasttrackit.dataprovider.itemdataprovider;

import org.fasttrackit.ItemData;
import org.testng.annotations.DataProvider;

import java.util.ArrayList;
import java.util.List;

public class DogItemDataProvider {
    static ItemData amlr = new ItemData("EST-22", "Adult Male Labrador Retriever");
    static ItemData aflr = new ItemData("EST-23", "Adult Female Labrador Retriever");

    @DataProvider(name = "labradorItem")
    public Object[][] getLabradorItem() {
        return new Object[][]{
                {amlr},
                {aflr},
        };
    }

    @DataProvider(name = "labradorItems")
    public Object[][] getLabradorItems() {
        List<ItemData> items = new ArrayList<>();
        items.add(amlr);
        items.add(aflr);
        return new Object[][]{
                {items},
        };
    }
}
