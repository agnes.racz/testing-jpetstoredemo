package org.fasttrackit.dataprovider;

import org.fasttrackit.Account;
import org.testng.annotations.DataProvider;

public class LoginDataProvider {

    static Account valid = new Account("j2ee", "j2ee");
    static Account noUserValPass = new Account("", "j2ee");
    static Account noUserInvalPass = new Account("", "j2eej2ee");
    static Account valUserNoPass = new Account("j2ee", "");
    static Account invalUserNoPass = new Account("asdf", "");
    static Account invalid = new Account("asdf", "asdf");
    static Account empty = new Account("", "");


    @DataProvider(name = "validLoginData")
    public Object[][] getValidLogin() {
        return new Object[][]{
                {valid}
        };
    }

    @DataProvider(name = "invalidLoginData")
    public Object[][] getInvalidLogin() {
        return new Object[][]{
                {noUserInvalPass},
                {noUserValPass},
                {valUserNoPass},
                {invalUserNoPass},
                {invalid},
                {empty}
        };
    }
}
