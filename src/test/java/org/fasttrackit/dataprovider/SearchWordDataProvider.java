package org.fasttrackit.dataprovider;

import org.fasttrackit.SearchWord;
import org.testng.annotations.DataProvider;

public class SearchWordDataProvider {
    static SearchWord fish = new SearchWord("fish", 2);
    static SearchWord shark = new SearchWord("shark", 1);
    static SearchWord poodle = new SearchWord("poodle", 1);
    static SearchWord retriever = new SearchWord("retriever", 2);
    static SearchWord snake = new SearchWord("snake", 1);
    static SearchWord parrot = new SearchWord("parrot", 1);
    static SearchWord bread = new SearchWord("bread", 0);
    static SearchWord backpack = new SearchWord("backpack", 0);
    static SearchWord candle = new SearchWord("candle", 0);
    static SearchWord mug = new SearchWord("mug", 0);

    @DataProvider(name = "searchWordForSubcategories")
    public Object[][] getSubcategorySearchWord() {
        return new Object[][]{
                {fish},
                {shark},
                {poodle},
                {retriever},
                {snake},
                {parrot},
        };
    }

    @DataProvider(name = "wrongSearchWord")
    public Object[][] getWrongSearchWord() {
        return new Object[][]{
                {bread},
                {backpack},
                {candle},
                {mug},
        };
    }
}
