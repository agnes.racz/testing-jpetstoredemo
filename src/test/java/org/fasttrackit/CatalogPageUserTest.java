package org.fasttrackit;

import io.qameta.allure.*;
import org.fasttrackit.config.Config;
import org.fasttrackit.dataprovider.itemdataprovider.FishItemDataProvider;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.closeWebDriver;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

@Feature("Elements on Catalog page - as logged in user")
public class CatalogPageUserTest extends Config {
    CatalogPage catalogPage = new CatalogPage();
    Header header = new Header();
    SignInPage signInPage = new SignInPage();

    @BeforeMethod
    public void setupSession() {
        catalogPage.openCatalogPage();
        header.clickOnSignInLink();
        signInPage.typeInUsername("j2ee");
        signInPage.typeInPassword("j2ee");
        signInPage.clickOnLoginButton();
    }

    @AfterMethod
    @Step("Close Web Driver")
    public static void closeSession() {
        closeWebDriver();
    }

    @Test(description = "Header is displayed on Catalog Page")
    @Description("Header is displayed in Catalog Page")
    @Story("Verifying if main elements are displayed on Catalog page")
    @Severity(SeverityLevel.CRITICAL)
    public void header_is_displayed_on_Catalog_page() {
        assertTrue(catalogPage.isHeaderDisplayed(), "Catalog page must contain Header");
    }

    @Test(description = "Main catalog is displayed on Catalog Page")
    @Description("Main Catalog is displayed in Catalog Page")
    @Story("Verifying if main elements are displayed on Catalog page")
    @Severity(SeverityLevel.CRITICAL)
    public void main_catalog_is_displayed_on_Catalog_page() {
        assertTrue(catalogPage.isMainCatalogDisplayed(), "Catalog page must contain Welcome Content");
    }

    @Test(description = "Footer is displayed on Catalog Page")
    @Description("Footer is displayed in Catalog Page")
    @Story("Verifying if main elements are displayed on Catalog page")
    @Severity(SeverityLevel.MINOR)
    public void footer_is_displayed_on_Catalog_page() {
        assertTrue(catalogPage.isFooterDisplayed(), "Catalog page must contain Footer");
    }

    @Test(description = "Fish button displayed in Sidebar")
    @Description("Fish button is displayed in Sidebar")
    @Story("Verifying if Category buttons are displayed in Sidebar")
    @Severity(SeverityLevel.CRITICAL)
    public void fish_button_is_displayed_in_Sidebar() {
        assertTrue(catalogPage.isFishButtonDisplayed(), "Sidebar must contain Fish button");
    }

    @Test(description = "Dogs button displayed in Sidebar")
    @Description("Dogs button is displayed in Sidebar")
    @Story("Verifying if Category buttons are displayed in Sidebar")
    @Severity(SeverityLevel.CRITICAL)
    public void dogs_button_is_displayed_in_Sidebar() {
        assertTrue(catalogPage.isDogsButtonDisplayed(), "Sidebar must contain Dogs button");
    }

    @Test(description = "Cats button displayed in Sidebar")
    @Description("Cats button is displayed in Sidebar")
    @Story("Verifying if Category buttons are displayed in Sidebar")
    @Severity(SeverityLevel.CRITICAL)
    public void cats_button_is_displayed_in_Sidebar() {
        assertTrue(catalogPage.isCatsButtonDisplayed(), "Sidebar must contain Cats button");
    }

    @Test(description = "Reptiles button displayed in Sidebar")
    @Description("Reptiles button is displayed in Sidebar")
    @Story("Verifying if Category buttons are displayed in Sidebar")
    @Severity(SeverityLevel.CRITICAL)
    public void reptiles_button_is_displayed_in_Sidebar() {
        assertTrue(catalogPage.isReptilesButtonDisplayed(), "Sidebar must contain Reptiles button");
    }

    @Test(description = "Birds button displayed in Sidebar")
    @Description("Birds button is displayed in Sidebar")
    @Story("Verifying if Category buttons are displayed in Sidebar")
    @Severity(SeverityLevel.CRITICAL)
    public void birds_button_is_displayed_in_Sidebar() {
        assertTrue(catalogPage.isBirdsButtonDisplayed(), "Sidebar must contain Birds button");
    }

    @Test(description = "Subcategories are displayed when opening Fish Category page")
    @Description("Subcategories are displayed when opening Fish page")
    @Story("Verifying if Subcategories are displayed")
    @Severity(SeverityLevel.CRITICAL)
    public void the_subcategories_are_displayed_when_opening_Fish_page() {
        header.clickOnFishButton();
        assertEquals(catalogPage.getSubcategoriesNumber(), 4, "The number of subcategories must be 4");
    }

    @Test(description = "Subcategories are displayed when opening Dogs Category page")
    @Description("Subcategories are displayed when opening Dogs page")
    @Story("Verifying if Subcategories are displayed")
    @Severity(SeverityLevel.CRITICAL)
    public void the_subcategories_are_displayed_when_opening_Dogs_page() {
        header.clickOnDogsButton();
        assertEquals(catalogPage.getSubcategoriesNumber(), 6, "The number of subcategories must be 6");
    }

    @Test(description = "Subcategories are displayed when opening Reptiles Category page")
    @Description("Subcategories are displayed when opening Reptiles page")
    @Story("Verifying if Subcategories are displayed")
    @Severity(SeverityLevel.CRITICAL)
    public void the_subcategories_are_displayed_when_opening_Reptiles_page() {
        header.clickOnReptilesButton();
        assertEquals(catalogPage.getSubcategoriesNumber(), 2, "The number of subcategories must be 2");
    }

    @Test(description = "Subcategories are displayed when opening Cats Category page")
    @Description("Subcategories are displayed when opening Cats page")
    @Story("Verifying if Subcategories are displayed")
    @Severity(SeverityLevel.CRITICAL)
    public void the_subcategories_are_displayed_when_opening_Cats_page() {
        header.clickOnCatsButton();
        assertEquals(catalogPage.getSubcategoriesNumber(), 2, "The number of subcategories must be 2");
    }

    @Test(description = "Subcategories are displayed when opening Birds Category page")
    @Description("Subcategories are displayed when opening Birds page")
    @Story("Verifying if Subcategories are displayed")
    @Severity(SeverityLevel.CRITICAL)
    public void the_subcategories_are_displayed_when_opening_Birds_page() {
        header.clickOnBirdsButton();
        assertEquals(catalogPage.getSubcategoriesNumber(), 2, "The number of subcategories must be 2");
    }

    @Test(description = "Return to Main Menu link is displayed on Fish Category page")
    @Description("Return to Main Menu link is displayed on Fish page")
    @Story("Verifying if Return links are displayed")
    @Severity(SeverityLevel.MINOR)
    public void return_to_Main_Menu_is_displayed_on_Fish_page() {
        header.clickOnFishButton();
        assertTrue(catalogPage.backLinkIsDisplayed(), "Return to Main Menu must be displayed");
    }

    @Test(description = "Angelfish subcategory table has 3 rows")
    @Description("Angelfish subcategory table has 3 rows")
    @Story("Verifying subcategories")
    @Severity(SeverityLevel.MINOR)
    public void the_Angelfish_subcategory_table_has_3_rows() {
        header.clickOnFishButton();
        catalogPage.clickOnAngelfishSubcategoryLink();
        assertEquals(catalogPage.getItemsTableSize(), 3, "The number of rows must be 3");
    }

    @Test(description = "Angelfish subcategory has 2 items")
    @Description("Angelfish subcategory has 2 items")
    @Story("Verifying subcategories")
    @Severity(SeverityLevel.MINOR)
    public void angelfish_subcategory_has_two_items() {
        header.clickOnFishButton();
        catalogPage.clickOnAngelfishSubcategoryLink();
        assertEquals(catalogPage.getItemsNumber(), 2, "The number of products must be 2");
    }

    @Test(dataProviderClass = FishItemDataProvider.class, dataProvider = "angelfishItem", description = "Item ID is displayed in Angelfish subcategory table")
    @Description("Item ID is displayed in Angelfish subcategory table")
    @Story("Verifying if item details are displayed")
    @Severity(SeverityLevel.NORMAL)
    public void itemID_is_displayed_on_Angelfish_page(ItemData itemData) {
        header.clickOnFishButton();
        catalogPage.clickOnAngelfishSubcategoryLink();
        CatalogItem catalogItem = new CatalogItem(itemData.getID());
        assertTrue(catalogItem.isItemIDDisplayed(), "Item ID must be displayed");
    }

    @Test(dataProviderClass = FishItemDataProvider.class, dataProvider = "koiItem", description = "Item ID is displayed in Koi subcategory table")
    @Description("Item ID is displayed in Koi subcategory table")
    @Story("Verifying if item details are displayed")
    @Severity(SeverityLevel.NORMAL)
    public void itemID_is_displayed_on_Koi_page(ItemData itemData) {
        header.clickOnFishButton();
        catalogPage.clickOnKoiSubcategoryLink();
        CatalogItem catalogItem = new CatalogItem(itemData.getID());
        assertTrue(catalogItem.isItemIDDisplayed(), "Item ID must be displayed");
    }

    @Test(dataProviderClass = FishItemDataProvider.class, dataProvider = "angelfishItem", description = "Product ID is displayed in Angelfish subcategory table")
    @Description("Product ID is displayed in Angelfish subcategory table")
    @Story("Verifying if item details are displayed")
    @Severity(SeverityLevel.NORMAL)
    public void productID_is_displayed_on_Angelfish_page(ItemData itemData) {
        header.clickOnFishButton();
        catalogPage.clickOnAngelfishSubcategoryLink();
        CatalogItem catalogItem = new CatalogItem(itemData.getID());
        assertTrue(catalogItem.isProductIDDisplayed(), "Product ID on Item page must be displayed");
    }

    @Test(dataProviderClass = FishItemDataProvider.class, dataProvider = "koiItem", description = "Product ID is displayed in Koi subcategory table")
    @Description("Product ID is displayed in Koi subcategory table")
    @Story("Verifying if item details are displayed")
    @Severity(SeverityLevel.NORMAL)
    public void productID_is_displayed_on_Koi_page(ItemData itemData) {
        header.clickOnFishButton();
        catalogPage.clickOnKoiSubcategoryLink();
        CatalogItem catalogItem = new CatalogItem(itemData.getID());
        assertTrue(catalogItem.isProductIDDisplayed(), "Product ID on Item page must be displayed");
    }

    @Test(dataProviderClass = FishItemDataProvider.class, dataProvider = "angelfishItem", description = "Item's product ID is equal to the Angelfish Subcategory Product ID")
    @Description("Product ID is equal to Angelfish subcategory Product ID")
    @Story("Verifying if item details are displayed")
    @Severity(SeverityLevel.NORMAL)
    public void productID_is_equal_to_Angelfish_productID(ItemData itemData) {
        header.clickOnFishButton();
        catalogPage.clickOnAngelfishSubcategoryLink();
        CatalogItem catalogItem = new CatalogItem(itemData.getID());
        assertEquals(catalogItem.getProductID(), "FI-SW-01", "The product ID of this item must be: FI-SW-01");
    }

    @Test(dataProviderClass = FishItemDataProvider.class, dataProvider = "koiItem", description = "Item's product ID is equal to the Koi Subcategory Product ID")
    @Description("Product ID is equal to Koi subcategory Product ID")
    @Story("Verifying if item details are displayed")
    @Severity(SeverityLevel.NORMAL)
    public void productID_is_equal_to_Koi_productID(ItemData itemData) {
        header.clickOnFishButton();
        catalogPage.clickOnKoiSubcategoryLink();
        CatalogItem catalogItem = new CatalogItem(itemData.getID());
        assertEquals(catalogItem.getProductID(), "FI-FW-01", "The product ID of this item must be: FI-FW-01");
    }

    @Test(dataProviderClass = FishItemDataProvider.class, dataProvider = "angelfishItem", description = "Item's description is displayed in Angelfish subcategory table")
    @Description("Item description is displayed in Angelfish subcategory table")
    @Story("Verifying if item details are displayed")
    @Severity(SeverityLevel.CRITICAL)
    public void item_description_is_displayed_on_Angelfish_page(ItemData itemData) {
        header.clickOnFishButton();
        catalogPage.clickOnAngelfishSubcategoryLink();
        CatalogItem catalogItem = new CatalogItem(itemData.getID());
        assertTrue(catalogItem.isDescriptionDisplayed(), "Description must be displayed");
    }

    @Test(dataProviderClass = FishItemDataProvider.class, dataProvider = "koiItem", description = "Item's description is displayed in Koi subcategory table")
    @Description("Item description is displayed in Koi subcategory table")
    @Story("Verifying if item details are displayed")
    @Severity(SeverityLevel.CRITICAL)
    public void item_description_is_displayed_on_Koi_page(ItemData itemData) {
        header.clickOnFishButton();
        catalogPage.clickOnKoiSubcategoryLink();
        CatalogItem catalogItem = new CatalogItem(itemData.getID());
        assertTrue(catalogItem.isDescriptionDisplayed(), "Description must be displayed");
    }

    @Test(dataProviderClass = FishItemDataProvider.class, dataProvider = "angelfishItem", description = "Item's price is displayed in Angelfish subcategory table")
    @Description("Item price is displayed in Angelfish subcategory table")
    @Story("Verifying if item details are displayed")
    @Severity(SeverityLevel.CRITICAL)
    public void item_price_is_displayed_on_Angelfish_page(ItemData itemData) {
        header.clickOnFishButton();
        catalogPage.clickOnAngelfishSubcategoryLink();
        CatalogItem catalogItem = new CatalogItem(itemData.getID());
        assertTrue(catalogItem.isPriceDisplayed(), "Price must be displayed");
    }

    @Test(dataProviderClass = FishItemDataProvider.class, dataProvider = "koiItem", description = "Item's price is displayed in Koi subcategory table")
    @Description("Item price is displayed in Koi subcategory table")
    @Story("Verifying if item details are displayed")
    @Severity(SeverityLevel.CRITICAL)
    public void item_price_is_displayed_on_Koi_page(ItemData itemData) {
        header.clickOnFishButton();
        catalogPage.clickOnKoiSubcategoryLink();
        CatalogItem catalogItem = new CatalogItem(itemData.getID());
        assertTrue(catalogItem.isPriceDisplayed(), "Price must be displayed");
    }

    @Test(dataProviderClass = FishItemDataProvider.class, dataProvider = "angelfishItem", description = "Item's Add to cart button is displayed in Angelfish subcategory table")
    @Description("Item's Add to cart button is displayed in Angelfish subcategory table")
    @Story("Verifying if item details are displayed")
    @Severity(SeverityLevel.BLOCKER)
    public void item_s_Add_to_Cart_Button_is_displayed_on_Angelfish_page(ItemData itemData) {
        header.clickOnFishButton();
        catalogPage.clickOnAngelfishSubcategoryLink();
        CatalogItem catalogItem = new CatalogItem(itemData.getID());
        assertTrue(catalogItem.isAddToCartButtonDisplayed(), "Add to Cart button must be displayed");
    }

    @Test(dataProviderClass = FishItemDataProvider.class, dataProvider = "koiItem", description = "Item's Add to cart button is displayed in Koi subcategory table")
    @Description("Item's Add to cart button is displayed in Koi subcategory table")
    @Story("Verifying if item details are displayed")
    @Severity(SeverityLevel.BLOCKER)
    public void item_s_Add_to_Cart_Button_is_displayed_on_Koi_page(ItemData itemData) {
        header.clickOnFishButton();
        catalogPage.clickOnKoiSubcategoryLink();
        CatalogItem catalogItem = new CatalogItem(itemData.getID());
        assertTrue(catalogItem.isAddToCartButtonDisplayed(), "Add to Cart button must be displayed");
    }
}

