package org.fasttrackit;

import io.qameta.allure.*;
import org.fasttrackit.config.Config;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.closeWebDriver;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

@Feature("Elements on Homepage")
public class HomepageStaticTest extends Config {
    Homepage homepage = new Homepage();

    @BeforeMethod
    public void setupSession() {
        homepage.openHomepage();
    }

    @AfterMethod
    @Step("Close Web Driver")
    public static void closeSession() {
        closeWebDriver();
    }

    @Test(description = "Welcome text is displayed in Header")
    @Description("Welcome text is displayed in Header")
    @Story("Verifying if elements are displayed on Homepage")
    @Severity(SeverityLevel.MINOR)
    public void welcome_text_is_displayed_on_Homepage() {
        assertEquals(homepage.getWelcomeText(), "Welcome to JPetStore 6", "When opening the homepage, welcome text is: Welcome to JPetStore 6");
    }

    @Test(description = "Enter the Store link is displayed in Header")
    @Description("Enter the Store link is displayed in Header")
    @Story("Verifying if elements are displayed on Homepage")
    @Severity(SeverityLevel.CRITICAL)
    public void enter_the_store_link_is_displayed_on_Homepage() {
        assertTrue(homepage.enterTheStoreLinkDisplayed(), "When opening the homepage, Enter the Store link is displayed");
    }

    @Test(description = "Copyright details are displayed in Header")
    @Description("Copyright details are displayed in Header")
    @Story("Verifying if elements are displayed on Homepage")
    @Severity(SeverityLevel.MINOR)
    public void copyright_details_are_displayed_on_Homepage() {
        assertTrue(homepage.copyrightDetailsAreDisplayed(), "When opening the homepage, copyright details are displayed");
    }
}
