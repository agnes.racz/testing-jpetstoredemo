package org.fasttrackit;

import io.qameta.allure.*;
import org.fasttrackit.config.Config;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.closeWebDriver;
import static org.testng.Assert.assertTrue;

@Feature("Elements in Header - as logged in user")
public class HeaderAsUserTest extends Config {

    CatalogPage catalogPage = new CatalogPage();
    Header header = new Header();
    SignInPage signInPage = new SignInPage();

    @BeforeClass
    public void openCatalogPage() {
        catalogPage.openCatalogPage();
        header.clickOnSignInLink();
        signInPage.typeInUsername("j2ee");
        signInPage.typeInPassword("j2ee");
        signInPage.clickOnLoginButton();
    }
    @AfterClass
    @Step("Close Web Driver")
    public static void closeSession() {
        closeWebDriver();
    }

    @Test(description = "Logo is displayed in Header")
    @Description("Logo is displayed in Header")
    @Story("Verifying if elements are displayed in Header")
    @Severity(SeverityLevel.MINOR)
    public void logo_is_displayed_in_Header() {
        assertTrue(header.logoIsDisplayed(), "Header must contain logo");
    }

    @Test(description = "Cart icon is displayed in Header")
    @Description("Cart icon is displayed in Header")
    @Story("Verifying if elements are displayed in Header")
    @Severity(SeverityLevel.CRITICAL)
    public void cart_icon_is_displayed_in_Header() {
        assertTrue(header.cartIconIsDisplayed(), "Header must contain cart icon");
    }

    @Test(description = "Question mark is displayed in Header")
    @Description("Question mark is displayed in Header")
    @Story("Verifying if elements are displayed in Header")
    @Severity(SeverityLevel.MINOR)
    public void question_mark_is_displayed_in_Header() {
        assertTrue(header.questionMarkIsDisplayed(), "Header must contain Question mark");
    }

    @Test(description = "Search bar is displayed in Header")
    @Description("Search bar is displayed in Header")
    @Story("Verifying if elements are displayed in Header")
    @Severity(SeverityLevel.CRITICAL)
    public void search_bar_is_displayed_in_Header() {
        assertTrue(header.searchBarIsDisplayed(), "Header must contain Search bar");
    }

    @Test(description = "Search button is displayed in Header")
    @Description("Search button is displayed in Header")
    @Story("Verifying if elements are displayed in Header")
    @Severity(SeverityLevel.CRITICAL)
    public void search_button_is_displayed_in_Header() {
        assertTrue(header.searchButtonIsDisplayed(), "Header must contain Search button");
    }

    @Test(description = "Quicklinks is displayed in Header")
    @Description("Quicklinks is displayed in Header")
    @Story("Verifying if elements are displayed in Header")
    @Severity(SeverityLevel.CRITICAL)
    public void quicklinks_is_displayed_in_Heather() {
        assertTrue(header.quicklinksIsDisplayed(), "Quicklinks must contain Fish button");
    }

    @Test(description = "Fish button is displayed in Header")
    @Description("Fish button is displayed in Header")
    @Story("Verifying if elements are displayed in Quicklinks")
    @Severity(SeverityLevel.CRITICAL)
    public void fish_button_is_displayed_in_Quicklinks() {
        assertTrue(header.fishButtonIsDisplayed(), "Quicklinks must contain Fish button");
    }

    @Test(description = "Dogs button is displayed in Header")
    @Description("Dogs button is displayed in Header")
    @Story("Verifying if elements are displayed in Quicklinks")
    @Severity(SeverityLevel.CRITICAL)
    public void dogs_button_is_displayed_in_Quicklinks() {
        assertTrue(header.dogsButtonIsDisplayed(), "Quicklinks must contain Dogs button");
    }

    @Test(description = "Reptiles button is displayed in Header")
    @Description("Reptiles button is displayed in Header")
    @Story("Verifying if elements are displayed in Quicklinks")
    @Severity(SeverityLevel.CRITICAL)
    public void reptiles_button_is_displayed_in_Quicklinks() {
        assertTrue(header.reptilesButtonIsDisplayed(), "Quicklinks must contain Reptiles button");
    }

    @Test(description = "Cats button is displayed in Header")
    @Description("Cats button is displayed in Header")
    @Story("Verifying if elements are displayed in Quicklinks")
    @Severity(SeverityLevel.CRITICAL)
    public void cats_button_is_displayed_in_Quicklinks() {
        assertTrue(header.catsButtonIsDisplayed(), "Quicklinks must contain Cats button");
    }

    @Test(description = "Birds button is displayed in Header")
    @Description("Birds button is displayed in Header")
    @Story("Verifying if elements are displayed in Quicklinks")
    @Severity(SeverityLevel.CRITICAL)
    public void birds_button_is_displayed_in_Quicklinks() {
        assertTrue(header.birdsButtonIsDisplayed(), "Quicklinks must contain Birds button");
    }
}

