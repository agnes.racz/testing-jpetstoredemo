package org.fasttrackit;

import io.qameta.allure.*;
import org.fasttrackit.config.Config;
import org.fasttrackit.dataprovider.LoginDataProvider;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.closeWebDriver;
import static org.testng.Assert.*;

@Feature("Functionality - Login")
public class LoginFunctionalityTest extends Config {
    CatalogPage catalogPage = new CatalogPage();
    Header header = new Header();
    SignInPage signInPage = new SignInPage();
    Footer footer = new Footer();

    @BeforeMethod
    public void setupSession() {
        catalogPage.openCatalogPage();
    }

    @AfterMethod
    @Step("Close Web Driver")
    public static void closeSession() {
        closeWebDriver();
    }

    @Test(description = "Username input field is empty on Sign in page")
    @Description("Username input field is empty on Sign in page")
    @Story("Verifying Sign in form elements")
    @Severity(SeverityLevel.NORMAL)
    public void username_input_field_is_empty_on_sign_in_page() {
        header.clickOnSignInLink();
        assertEquals(signInPage.getUsernameInputText(), "", "Username input field must be empty");
    }

    @Test(description = "Password input field is empty on Sign in page")
    @Description("Password input field is empty on Sign in page")
    @Story("Verifying Sign in form elements")
    @Severity(SeverityLevel.NORMAL)
    public void password_input_field_is_empty_on_sign_in_page() {
        header.clickOnSignInLink();
        assertEquals(signInPage.getPasswordInputText(), "", "Password input field must be empty");
    }

    @Test(dataProviderClass = LoginDataProvider.class, dataProvider = "validLoginData", description = "User can login with valid credentials, by clicking on Login button")
    @Description("User can login with valid credentials by clicking on login")
    @Story("Login with valid credentials")
    @Severity(SeverityLevel.BLOCKER)
    public void user_can_login_with_valid_credentials_by_click(Account account) {
        header.clickOnSignInLink();
        signInPage.typeInUsername(account.getUsername());
        signInPage.typeInPassword(account.getPassword());
        signInPage.clickOnLoginButton();
        assertEquals(catalogPage.getWelcomeMessage(), "Welcome ABC!", "Welcome message must be: Welcome ABC!");
    }

    @Test(dataProviderClass = LoginDataProvider.class, dataProvider = "validLoginData", description = "User can login with valid credentials, by pressing Enter")
    @Description("User can login with valid credentials by pressing Enter")
    @Story("Login with valid credentials")
    @Severity(SeverityLevel.NORMAL)
    public void user_can_login_with_valid_credentials_by_pressing_enter(Account account) {
        header.clickOnSignInLink();
        signInPage.typeInUsername(account.getUsername());
        signInPage.typeInPassword(account.getPassword());
        signInPage.pressEnterOnLoginButton();
        assertEquals(catalogPage.getWelcomeMessage(), "Welcome ABC!", "Welcome message must be: Welcome ABC!");
    }

    @Test(description = "Sign out link is displayed after Login")
    @Description("Sign out link is displayed in Header after Login")
    @Story("Verifying elements after Login")
    @Severity(SeverityLevel.NORMAL)
    public void sign_out_link_is_displayed_in_Header_after_login() {
        header.clickOnSignInLink();
        signInPage.typeInUsername("j2ee");
        signInPage.typeInPassword("j2ee");
        signInPage.clickOnLoginButton();
        assertTrue(header.isSignOutLinkDisplayed(), "Sign out link must be displayed in Header after Login");
    }

    @Test(description = "My Account link is displayed after Login")
    @Description("My Account link is displayed in Header after Login")
    @Story("Verifying elements after Login")
    @Severity(SeverityLevel.NORMAL)
    public void my_account_link_is_displayed_in_Header_after_login() {
        header.clickOnSignInLink();
        signInPage.typeInUsername("j2ee");
        signInPage.typeInPassword("j2ee");
        signInPage.clickOnLoginButton();
        assertTrue(header.isMyAccountLinkDisplayed(), "My Account link must be displayed in Header after Login");
    }

    @Test(description = "Stay Fit banner is displayed in Footer after Login")
    @Description("Stay Fit banner is displayed in Footer after Login")
    @Story("Verifying elements after Login")
    @Severity(SeverityLevel.MINOR)
    public void stay_fit_banner_is_displayed_in_Footer_after_login() {
        header.clickOnSignInLink();
        signInPage.typeInUsername("j2ee");
        signInPage.typeInPassword("j2ee");
        signInPage.clickOnLoginButton();
        assertTrue(footer.isStayFitBannerDisplayed(), "Stay Fit banner must be displayed in Footer after Login");
    }

    @Test(dataProviderClass = LoginDataProvider.class, dataProvider = "invalidLoginData", description = "Login unsuccessful with invalid credentials")
    @Description("Login unsuccessful with invalid username or invalid password.")
    @Story("Login with invalid credentials")
    @Severity(SeverityLevel.CRITICAL)
    public void login_unsuccessful_with_invalid_credentials(Account account) {
        header.clickOnSignInLink();
        signInPage.typeInUsername(account.getUsername());
        signInPage.typeInPassword(account.getPassword());
        signInPage.clickOnLoginButton();
        assertFalse(header.isSignOutLinkDisplayed(), "In Header Sign In link must not change to Sign Out");
    }
}
