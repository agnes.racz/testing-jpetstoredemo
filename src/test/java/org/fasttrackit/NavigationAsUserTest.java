package org.fasttrackit;

import io.qameta.allure.*;
import org.fasttrackit.config.Config;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.closeWebDriver;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;
import static org.testng.Assert.assertEquals;

@Feature("Navigation - as logged in user")
public class NavigationAsUserTest extends Config {
    CatalogPage catalogPage = new CatalogPage();
    Header header = new Header();
    CartPage cartPage = new CartPage();
    SignInPage signInPage = new SignInPage();

    @BeforeMethod
    public void setupSession() {
        catalogPage.openCatalogPage();
        header.clickOnSignInLink();
        signInPage.typeInUsername("j2ee");
        signInPage.typeInPassword("j2ee");
        signInPage.clickOnLoginButton();
    }

    @AfterMethod
    @Step("Close Web Driver")
    public static void closeSession() {
        closeWebDriver();
    }

    @Test(description = "User can navigate to Cart page from Header")
    @Description("User can navigate to Cart page by clicking on the cart icon in Header")
    @Story("Navigation from Header")
    @Severity(SeverityLevel.CRITICAL)
    public void user_can_navigate_to_cart_page_by_clicking_on_cart_icon_in_Heather() {
        header.clickOnCartIcon();
        assertEquals(cartPage.getPageTitle(), "Shopping Cart", "Page title must be: Shopping cart");
    }


    @Test(description = "User can navigate to JPetStore Demo description page from Header")
    @Description("User can navigate to JPetStore Demo description page by clicking the question mark in Header")
    @Story("Navigation from Header")
    @Severity(SeverityLevel.MINOR)
    public void user_can_navigate_to_JPetStore_Demo_description_page_by_clicking_on_question_mark_in_Header() {
        header.clickOnQuestionMark();
        assertEquals(getWebDriver().getCurrentUrl(), "https://petstore.octoperf.com/help.html", "URL must change to: https://petstore.octoperf.com/help.html");
    }

    @Test(description = "User can navigate to Fish category from Quicklinks")
    @Description("User can navigate to Fish category by clicking on the Fish button in Quicklinks")
    @Story("Navigation from Quicklinks")
    @Severity(SeverityLevel.CRITICAL)
    public void user_can_navigate_to_Fish_page_by_clicking_on_Fish_button_in_Header() {
        header.clickOnFishButton();
        assertEquals(catalogPage.getPageTitle(), "Fish", "Page title must be: Fish");
    }

    @Test(description = "User can navigate to Dogs category from Quicklinks")
    @Description("User can navigate to Dogs category by clicking on the Fish button in Quicklinks")
    @Story("Navigation from Quicklinks")
    @Severity(SeverityLevel.CRITICAL)
    public void user_can_navigate_to_Dogs_page_by_clicking_on_Dogs_button_in_Header() {
        header.clickOnDogsButton();
        assertEquals(catalogPage.getPageTitle(), "Dogs", "Page title must be: Dogs");
    }

    @Test(description = "User can navigate to Reptiles category from Quicklinks")
    @Description("User can navigate to Reptiles category by clicking on the Fish button in Quicklinks")
    @Story("Navigation from Quicklinks")
    @Severity(SeverityLevel.CRITICAL)
    public void user_can_navigate_to_Reptiles_page_by_clicking_on_Reptiles_button_in_Header() {
        header.clickOnReptilesButton();
        assertEquals(catalogPage.getPageTitle(), "Reptiles", "Page title must be: Reptiles");
    }

    @Test(description = "User can navigate to Cats category from Quicklinks")
    @Description("User can navigate to Cats category by clicking on the Fish button in Quicklinks")
    @Story("Navigation from Quicklinks")
    @Severity(SeverityLevel.CRITICAL)
    public void user_can_navigate_to_Cats_page_by_clicking_on_Cats_button_in_Header() {
        header.clickOnCatsButton();
        assertEquals(catalogPage.getPageTitle(), "Cats", "Page title must be: Cats");
    }

    @Test(description = "User can navigate to Birds category from Quicklinks")
    @Description("User can navigate to Birds category by clicking on the Fish button in Quicklinks")
    @Story("Navigation from Quicklinks")
    @Severity(SeverityLevel.CRITICAL)
    public void user_can_navigate_to_Birds_page_by_clicking_on_Birds_button_in_Header() {
        header.clickOnBirdsButton();
        assertEquals(catalogPage.getPageTitle(), "Birds", "Page title must be: Birds");
    }

    @Test(description = "User can navigate to Fish category from Sidebar")
    @Description("User can navigate to Fish category by clicking on the Fish button in Sidebar")
    @Story("Navigation from Sidebar")
    @Severity(SeverityLevel.CRITICAL)
    public void user_can_navigate_to_Fish_page_by_clicking_on_Fish_button_in_Sidebar() {
        catalogPage.clickOnFishButton();
        assertEquals(catalogPage.getPageTitle(), "Fish", "Page title must be Fish");
    }

    @Test(description = "User can navigate to Dogs category from Sidebar")
    @Description("User can navigate to Dogs category by clicking on the Fish button in Sidebar")
    @Story("Navigation from Sidebar")
    @Severity(SeverityLevel.CRITICAL)
    public void user_can_navigate_to_Dogs_page_by_clicking_on_Dogs_button_in_Sidebar() {
        catalogPage.clickOnDogsButton();
        assertEquals(catalogPage.getPageTitle(), "Dogs", "Page title must be Dogs");
    }

    @Test(description = "User can navigate to Cats category from Sidebar")
    @Description("User can navigate to Cats category by clicking on the Fish button in Sidebar")
    @Story("Navigation from Sidebar")
    @Severity(SeverityLevel.CRITICAL)
    public void user_can_navigate_to_Cats_page_by_clicking_on_Cats_button_in_Sidebar() {
        catalogPage.clickOnCatsButton();
        assertEquals(catalogPage.getPageTitle(), "Cats", "Page title must be Cats");
    }

    @Test(description = "User can navigate to Reptiles category from Sidebar")
    @Description("User can navigate to Reptiles category by clicking on the Fish button in Sidebar")
    @Story("Navigation from Sidebar")
    @Severity(SeverityLevel.CRITICAL)
    public void user_can_navigate_to_Reptiles_page_by_clicking_on_Reptiles_button_in_Sidebar() {
        catalogPage.clickOnReptilesButton();
        assertEquals(catalogPage.getPageTitle(), "Reptiles", "Page title must be Reptiles");
    }

    @Test(description = "User can navigate to Birds category from Sidebar")
    @Description("User can navigate to Birds category by clicking on the Fish button in Sidebar")
    @Story("Navigation from Sidebar")
    @Severity(SeverityLevel.CRITICAL)
    public void user_can_navigate_to_Birds_page_by_clicking_on_Birds_button_in_Sidebar() {
        catalogPage.clickOnBirdsButton();
        assertEquals(catalogPage.getPageTitle(), "Birds", "Page title must be Birds");
    }

    @Test(description = "User can navigate to Main Menu from Fish category")
    @Description("User can navigate to Main Menu from Fish category")
    @Story("Navigation from Categories")
    @Severity(SeverityLevel.CRITICAL)
    public void user_can_navigate_to_Main_Menu_from_Fish_page() {
        header.clickOnFishButton();
        catalogPage.clickOnBackLink();
        assertEquals(getWebDriver().getCurrentUrl(), "https://petstore.octoperf.com/actions/Catalog.action", "URL must change to: https://petstore.octoperf.com/actions/Catalog.action");
    }

    @Test(description = "User can navigate to Angelfish subcategory from Fish category")
    @Description("User can navigate to Angelfish subcategory from Fish category")
    @Story("Navigation from Categories")
    @Severity(SeverityLevel.CRITICAL)
    public void user_can_navigate_to_Angelfish_page_from_Fish_page() {
        header.clickOnFishButton();
        catalogPage.clickOnAngelfishSubcategoryLink();
        assertEquals(catalogPage.getPageTitle(), "Angelfish", "The page title must be Angelfish");
    }

    @Test(description = "User can navigate to Tiger Shark subcategory from Fish category")
    @Description("User can navigate to Tiger Shark subcategory from Fish category")
    @Story("Navigation from Categories")
    @Severity(SeverityLevel.CRITICAL)
    public void user_can_navigate_to_TigerShark_page_from_Fish_page() {
        header.clickOnFishButton();
        catalogPage.clickOnTigerSharkSubcategoryLink();
        assertEquals(catalogPage.getPageTitle(), "Tiger Shark", "The page title must be Tiger Shark");
    }

    @Test(description = "User can navigate to Large Angelfish item page from Angelfish subcategory")
    @Description("User can navigate to Large Angelfish item page from Angelfish subcategory")
    @Story("Navigation from Subcategories")
    @Severity(SeverityLevel.NORMAL)
    public void user_can_navigate_to_Large_Angelfish_page_from_Angelfish_page() {
        header.clickOnFishButton();
        catalogPage.clickOnAngelfishSubcategoryLink();
        CatalogItem catalogItem = new CatalogItem("EST-1");
        catalogItem.clickOnItemID();
        assertEquals(catalogPage.getBackLink(), "Return to FI-SW-01", "Back Link must say: Return to FI-SW-01");
    }

    @Test(description = "User can navigate to Toothless Tiger Shark item page from Tiger Shark subcategory")
    @Description("User can navigate to Toothless Tiger Shark item page from Tiger Shark subcategory")
    @Story("Navigation from Subcategories")
    @Severity(SeverityLevel.NORMAL)
    public void user_can_navigate_to_Toothless_Tiger_Shark_page_from_Tiger_Shark_page() {
        header.clickOnFishButton();
        catalogPage.clickOnTigerSharkSubcategoryLink();
        CatalogItem catalogItem = new CatalogItem("EST-3");
        catalogItem.clickOnItemID();
        assertEquals(catalogPage.getBackLink(), "Return to FI-SW-02", "Back Link must say: Return to FI-SW-02");
    }

    @Test(description = "User can navigate to Catalog page from Cart page, by clicking on Return to Main link")
    @Description("User can navigate to Catalog page from Cart page, by clicking on Return to Main link")
    @Story("Navigation from Cart page")
    @Severity(SeverityLevel.NORMAL)
    public void user_can_navigate_to_Catalog_page_from_Cart_page_by_clicking_on_Return_to_Main_link() {
        header.clickOnCartIcon();
        cartPage.clickOnReturnToMainMenuLink();
        assertEquals(getWebDriver().getCurrentUrl(), "https://petstore.octoperf.com/actions/Catalog.action", "URL must change to: https://petstore.octoperf.com/actions/Catalog.action");
    }

    @Test(description = "User can navigate to Catalog page from Cart page, by clicking on the E-shop Logo in Header")
    @Description("User can navigate to Catalog page from Cart page, by clicking on E-shop Logo in Header")
    @Story("Navigation from Cart page")
    @Severity(SeverityLevel.NORMAL)
    public void user_can_navigate_to_Catalog_page_from_Cart_page_by_clicking_on_the_logo_in_Header(){
        header.clickOnCartIcon();
        header.clickOnLogo();
        assertEquals(getWebDriver().getCurrentUrl(), "https://petstore.octoperf.com/actions/Catalog.action", "URL must change to: https://petstore.octoperf.com/actions/Catalog.action");
    }

}
