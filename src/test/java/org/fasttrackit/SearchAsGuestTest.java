package org.fasttrackit;

import com.codeborne.selenide.WebDriverRunner;
import io.qameta.allure.*;
import org.fasttrackit.config.Config;
import org.fasttrackit.dataprovider.SearchWordDataProvider;
import org.fasttrackit.dataprovider.itemdataprovider.FishItemDataProvider;
import org.testng.annotations.*;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

@Feature("Functionality - Search - as guest")
public class SearchAsGuestTest extends Config {
    CatalogPage catalogPage = new CatalogPage();
    Header header = new Header();
    SearchPage searchPage = new SearchPage();

    @BeforeMethod
    @Step("Open Catalog Page")
    public void setupSession() {
        catalogPage.openCatalogPage();
    }

    @AfterMethod
    @Step("Close Web Driver")
    public static void closeSession() {
        WebDriverRunner.closeWebDriver();
    }

    @Test(dataProviderClass = SearchWordDataProvider.class, dataProvider = "searchWordForSubcategories", description = "Verifying if the result table contains the correct amount of subcategories in case of valid subcategory search word")
    @Description("Verifying if the result table contains the correct amount of subcategories in case of valid subcategory search word")
    @Story("Verifying search results")
    @Severity(SeverityLevel.NORMAL)
    public void the_result_table_contains_the_correct_amount_of_subcategories_when_searching_for_valid_subcategory_words(SearchWord searchWord) {
        header.typeInSearchWord(searchWord.getWord());
        header.clickOnSearchButton();
        assertEquals(searchPage.getTotalNumberOfElementsReturned(), searchWord.getExpectedReturnAmount(), "The total number of returned elements must be " + searchWord.getExpectedReturnAmount());
    }

    @Test(dataProviderClass = SearchWordDataProvider.class, dataProvider = "wrongSearchWord", description = "Verifying if search result table is empty in case of invalid search word")
    @Description("Verifying if search result table in empty in case of invalid search word")
    @Story("Verifying search results")
    @Severity(SeverityLevel.NORMAL)
    public void the_result_table_contains_0_items_when_searching_for_invalid_words(SearchWord searchWord) {
        header.typeInSearchWord(searchWord.getWord());
        header.clickOnSearchButton();
        assertEquals(searchPage.getTotalNumberOfElementsReturned(), searchWord.getExpectedReturnAmount(), "The total number of returned elements must be " + searchWord.getExpectedReturnAmount());
    }

    @Test(description = "Verifying if search result subcategory names contain \"fish\"")
    @Description("Verifying if search result subcategory names contain \"fish\"")
    @Story("Verifying search results")
    @Severity(SeverityLevel.NORMAL)
    public void the_result_table_contains_the_correct_subcategories_when_searching_for_fish() {
        String searchWord = "fish";
        header.typeInSearchWord(searchWord);
        header.clickOnSearchButton();
        SearchResult result1 = new SearchResult("FI-FW-02");
        SearchResult result2 = new SearchResult("FI-SW-01");
        assertTrue(result1.isNameContaining(searchWord), "Subcategory name must contain: " + searchWord);
        assertTrue(result2.isNameContaining(searchWord), "Subcategory name must contain: " + searchWord);
    }

    @Test(description = "Guest can navigate to items page by clicking on Goldfish Product ID in search result")
    @Description("Guest can navigate to items page by clicking on Goldfish Product ID in search result")
    @Story("Guest interaction with search results")
    @Severity(SeverityLevel.NORMAL)
    public void guest_can_navigate_to_items_page_by_clicking_on_Goldfish_ProductID_in_search_result() {
        String searchWord = "fish";
        header.typeInSearchWord(searchWord);
        header.clickOnSearchButton();
        SearchResult result1 = new SearchResult("FI-FW-02");
        result1.clickOnProductID();
        assertEquals(catalogPage.getPageTitle(), "Goldfish", "Page title must be \"Goldfish\"");
    }

    @Test(dataProviderClass = FishItemDataProvider.class, dataProvider = "goldfishItem", description = "Cart contains item added by guest after searching for \"fish\"")
    @Description("Cart contains item added by guest after searching for \"fish\"")
    @Story("Guest interaction with search results")
    @Severity(SeverityLevel.CRITICAL)
    public void cart_contains_item_added_by_guest_after_searching_for_fish(ItemData itemData) {
        String searchWord = "fish";
        header.typeInSearchWord(searchWord);
        header.clickOnSearchButton();
        SearchResult result1 = new SearchResult("FI-FW-02");
        result1.clickOnProductID();
        CatalogItem catalogItem = new CatalogItem(itemData.getID());
        catalogItem.clickOnAddToCartButton();
        CartItem cartItem = new CartItem(itemData.getID());
        assertEquals(cartItem.getCartItemID(), itemData.getID(), "The cart must contain item with ID " + itemData.getID());
    }
}
