package org.fasttrackit;

import com.codeborne.selenide.WebDriverRunner;
import io.qameta.allure.*;
import org.fasttrackit.config.Config;
import org.fasttrackit.dataprovider.SearchWordDataProvider;
import org.fasttrackit.dataprovider.itemdataprovider.FishItemDataProvider;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

@Feature("Functionality - Search - as logged in user")
public class SearchAsUserTest extends Config {
    CatalogPage catalogPage = new CatalogPage();
    Header header = new Header();
    SearchPage searchPage = new SearchPage();
    SignInPage signInPage = new SignInPage();
    CartPage cartPage = new CartPage();
    CheckOutPage checkOutPage = new CheckOutPage();

    @BeforeMethod
    @Step("Open Catalog Page")
    public void setupSession() {
        catalogPage.openCatalogPage();
        header.clickOnSignInLink();
        signInPage.typeInUsername("j2ee");
        signInPage.typeInPassword("j2ee");
        signInPage.clickOnLoginButton();
    }

    @AfterMethod
    @Step("Close Web Driver")
    public static void closeSession() {
        WebDriverRunner.closeWebDriver();
    }

    @Test(description = "Verifying if search result subcategory names contain \"fish\"")
    @Description("Verifying if search result subcategory names contain \"fish\"")
    @Story("Verifying search results")
    @Severity(SeverityLevel.NORMAL)
    public void the_result_table_contains_the_correct_subcategories_when_searching_for_fish_as_logged_in_user() {
        String searchWord = "fish";
        header.typeInSearchWord(searchWord);
        header.clickOnSearchButton();
        SearchResult result1 = new SearchResult("FI-FW-02");
        SearchResult result2 = new SearchResult("FI-SW-01");
        assertTrue(result1.isNameContaining(searchWord), "Subcategory name must contain: " + searchWord);
        assertTrue(result2.isNameContaining(searchWord), "Subcategory name must contain: " + searchWord);
    }

    @Test(dataProviderClass = FishItemDataProvider.class, dataProvider = "goldfishItem", description = "Cart contains item added by logged in user after searching for \"fish\"")
    @Description("Cart contains item added by logged in user after searching for \"fish\"")
    @Story("Logged in user interaction with search results")
    @Severity(SeverityLevel.CRITICAL)
    public void cart_contains_item_added_by_logged_in_user_after_searching_for_fish(ItemData itemData) {
        header.typeInSearchWord("fish");
        header.clickOnSearchButton();
        SearchResult result = new SearchResult("FI-FW-02");
        result.clickOnProductID();
        CatalogItem catalogItem = new CatalogItem(itemData.getID());
        catalogItem.clickOnAddToCartButton();
        CartItem cartItem = new CartItem(itemData.getID());
        assertEquals(cartItem.getCartItemID(), itemData.getID(), "The cart must contain item with ID " + itemData.getID());
    }

    @Test(dataProviderClass = SearchWordDataProvider.class, dataProvider = "searchWordForSubcategories", description = "Verifying if the result table contains the correct amount of subcategories in case of valid subcategory search word")
    @Description("Verifying if the result table contains the correct amount of subcategories in case of valid subcategory search word")
    @Story("Verifying search results")
    @Severity(SeverityLevel.NORMAL)
    public void the_result_table_contains_the_correct_amount_of_subcategories_when_searching_for_valid_subcategory_words_as_logged_in_user(SearchWord searchWord) {
        header.typeInSearchWord(searchWord.getWord());
        header.clickOnSearchButton();
        assertEquals(searchPage.getTotalNumberOfElementsReturned(), searchWord.getExpectedReturnAmount(), "The total number of returned elements must be " + searchWord.getExpectedReturnAmount());
    }

    @Test(dataProviderClass = SearchWordDataProvider.class, dataProvider = "wrongSearchWord", description = "Verifying if search result table in empty in case of invalid search word")
    @Description("Verifying if search result table in empty in case of invalid search word")
    @Story("Verifying search results")
    @Severity(SeverityLevel.NORMAL)
    public void the_result_table_contains_0_items_when_searching_for_invalid_words_as_logged_in_user(SearchWord searchWord) {
        header.typeInSearchWord(searchWord.getWord());
        header.clickOnSearchButton();
        assertEquals(searchPage.getTotalNumberOfElementsReturned(), searchWord.getExpectedReturnAmount(), "The total number of returned elements must be " + searchWord.getExpectedReturnAmount());
    }


    @Test(description = "Logged in user can navigate to items page by clicking on Goldfish Product ID in search result")
    @Description("Logged in user can navigate to items page by clicking on Goldfish Product ID in search result")
    @Story("Logged in user interaction with search results")
    @Severity(SeverityLevel.NORMAL)
    public void logged_in_user_can_navigate_to_items_page_by_clicking_on_Goldfish_ProductID_in_search_result() {
        String searchWord = "fish";
        header.typeInSearchWord(searchWord);
        header.clickOnSearchButton();
        SearchResult result1 = new SearchResult("FI-FW-02");
        result1.clickOnProductID();
        assertEquals(catalogPage.getPageTitle(), "Goldfish", "Page title must be \"Goldfish\"");
    }

    @Test(dataProviderClass = FishItemDataProvider.class, dataProvider = "goldfishItem", description = "Check out form appears when a logged in user proceeds to check out, after searching for \"fish\"")
    @Description("Check out form appears when a logged in user proceeds to check out, after searching for \"fish\"")
    @Story("Logged in user interaction with search results")
    @Severity(SeverityLevel.CRITICAL)
    public void check_out_form_appears_when_logged_in_user_proceeds_to_checkout_after_search(ItemData itemData) {
        String searchWord = "fish";
        header.typeInSearchWord(searchWord);
        header.clickOnSearchButton();
        SearchResult result = new SearchResult("FI-FW-02");
        result.clickOnProductID();
        CatalogItem catalogItem = new CatalogItem(itemData.getID());
        catalogItem.clickOnAddToCartButton();
        cartPage.clickOnCheckoutButton();
        assertTrue(checkOutPage.isCheckOutFormDisplayed(), "Check out form must be displayed");
    }

    @Test(dataProviderClass = FishItemDataProvider.class, dataProvider = "goldfishItem", description = "Confirm button appears after clicking the continue button under the check out form")
    @Description("Confirm button appears after clicking the continue button under the check out form")
    @Story("Logged in user interaction with search results")
    @Severity(SeverityLevel.CRITICAL)
    public void confirm_button_appears_after_clicking_continue(ItemData itemData) {
        String searchWord = "fish";
        header.typeInSearchWord(searchWord);
        header.clickOnSearchButton();
        SearchResult result = new SearchResult("FI-FW-02");
        result.clickOnProductID();
        CatalogItem catalogItem = new CatalogItem(itemData.getID());
        catalogItem.clickOnAddToCartButton();
        cartPage.clickOnCheckoutButton();
        checkOutPage.clickOnContinueButton();
        assertTrue(checkOutPage.isConfirmButtonDisplayed(), "Confirm button must be displayed");
    }

    @Test(dataProviderClass = FishItemDataProvider.class, dataProvider = "goldfishItem", description = "Confirmation message appears, after logged in user confirms check out details")
    @Description("Confirmation message appears, after logged in user confirms check out details ")
    @Story("Logged in user interaction with search results")
    @Severity(SeverityLevel.CRITICAL)
    public void confirmation_message_appears_after_confirming_check_out_details(ItemData itemData) {
        String searchWord = "fish";
        header.typeInSearchWord(searchWord);
        header.clickOnSearchButton();
        SearchResult result = new SearchResult("FI-FW-02");
        result.clickOnProductID();
        CatalogItem catalogItem = new CatalogItem(itemData.getID());
        catalogItem.clickOnAddToCartButton();
        cartPage.clickOnCheckoutButton();
        checkOutPage.clickOnContinueButton();
        checkOutPage.clickOnConfirmButton();
        assertEquals(checkOutPage.getConfirmationMessage(), "Thank you, your order has been submitted.", "Confirmation message must be: Thank you, your order has been submitted.");
    }
}
