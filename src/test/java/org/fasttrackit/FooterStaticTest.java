package org.fasttrackit;

import io.qameta.allure.*;
import org.fasttrackit.config.Config;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

@Feature("Elements in Footer")
public class FooterStaticTest extends Config {

    CatalogPage catalogPage = new CatalogPage();
    Footer footer = new Footer();

    @BeforeClass
    public void openCatalogPage() {
        catalogPage.openCatalogPage();
    }

    @Test(description = "Creator link is displayed in Footer")
    @Description("Creator link is displayed in Footer")
    @Story("Verifying if Footer contains elements")
    @Severity(SeverityLevel.MINOR)
    public void creator_link_is_displayed_in_Footer() {
        assertTrue(footer.isCreatorLinkDisplayed(), "Footer must contain creator link");
    }

}