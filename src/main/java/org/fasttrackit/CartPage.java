package org.fasttrackit;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class CartPage {
    private final SelenideElement returnToMainMenuLink = $("#BackLink");
    private final SelenideElement pageTitle = $("#Catalog h2");
    private final ElementsCollection productsTable = $$("tr");
    private final SelenideElement emptyCartRow = $("tr:nth-of-type(2)"); // did not use colspan=8, because in case of a not working Remove button, the test would be blocked (the selector would have not been found)
    private final SelenideElement updateCartButton = $("[name='updateCartQuantities']");
    private final SelenideElement subTotal = updateCartButton.parent();
    private final SelenideElement checkoutButton = $("[href='/actions/Order.action?newOrderForm=']");

    public int getCartItemsNumber() {
        return productsTable.size() - 2;
    }

    public String getPageTitle() {
        return pageTitle.text();
    }

    public String getEmptyCartMessage() {
        return emptyCartRow.text();

    }

    public Double getSubTotal() {
        String subTotalText = subTotal.text().substring(12);
        return Double.parseDouble(subTotalText);
    }

    @Step("Click on Update Cart button")
    public void clickOnUpdateCartButton() {
        updateCartButton.click();
    }

    @Step("Click on Return to Main Menu link")
    public void clickOnReturnToMainMenuLink() {
        returnToMainMenuLink.click();
    }

    @Step("Click on Checkout button")
    public void clickOnCheckoutButton() {
        checkoutButton.click();
    }
}
