package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;

public class SearchResult {
    private final SelenideElement productID;
    private final SelenideElement descriptionLink;
    private final String name;

    public SearchResult(String productIDin) {
        String productIDSelector = String.format("b [href='/actions/Catalog.action?viewProduct=&productId=%s']", productIDin);
        this.productID = $(productIDSelector);
        this.descriptionLink = productID.parent().parent().$("a");
        this.name = productID.parent().parent().sibling(0).text();
    }

    public String getName() {
        return name;
    }

    public boolean isNameContaining(String word) {
        return name.contains(word);
    }

    @Step("Click on ProductID")
    public void clickOnProductID() {
        productID.click();
    }
}
