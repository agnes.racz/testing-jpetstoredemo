package org.fasttrackit;

public class SearchWord {
    private final String word;
    private final int expectedReturnAmount;

    public SearchWord(String word, int expectedReturnAmount) {
        this.word = word;
        this.expectedReturnAmount = expectedReturnAmount;
    }

    public String getWord() {
        return word;
    }

    public int getExpectedReturnAmount() {
        return expectedReturnAmount;
    }

    @Override
    public String toString() {
        return word;
    }
}
