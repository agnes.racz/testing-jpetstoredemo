package org.fasttrackit;

import com.codeborne.selenide.ElementsCollection;

import static com.codeborne.selenide.Selenide.$$;

public class SearchPage {

    private final ElementsCollection resultTable = $$("tr");

    public int getTotalNumberOfElementsReturned() {
        return resultTable.size() - 2;
    }
}
