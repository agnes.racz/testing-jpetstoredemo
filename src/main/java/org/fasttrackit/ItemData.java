package org.fasttrackit;

public class ItemData {
    private final String ID;
    private final String name;

    public ItemData(String id, String name) {
        this.ID = id;
        this.name = name;
    }

    public String getID() {
        return ID;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }
}
