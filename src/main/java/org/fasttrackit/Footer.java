package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class Footer {
    private final SelenideElement creatorLink = $("#PoweredBy");
    private final SelenideElement stayFitBanner = $("[src='../images/banner_dogs.gif']");

    public boolean isCreatorLinkDisplayed() {
        return creatorLink.isDisplayed();
    }

    public boolean isStayFitBannerDisplayed() {
        return stayFitBanner.isDisplayed();
    }
}
