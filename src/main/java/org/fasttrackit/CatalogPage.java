package org.fasttrackit;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.*;

public class CatalogPage {
    private final String catalogPageURL = "https://petstore.octoperf.com/actions/Catalog.action";
    private final SelenideElement header = $("#Header");
    private final SelenideElement welcomeContent = $("#WelcomeContent");
    private final SelenideElement mainCatalog = $("#Main");
    private final SelenideElement footer = $("#Footer");
    private final SelenideElement fishButton = $("#Sidebar [src='../images/fish_icon.gif']");
    private final SelenideElement dogsButton = $("#Sidebar [src='../images/dogs_icon.gif']");
    private final SelenideElement catsButton = $("#Sidebar [src='../images/cats_icon.gif']");
    private final SelenideElement reptilesButton = $("#Sidebar [src='../images/reptiles_icon.gif']");
    private final SelenideElement birdsButton = $("#Sidebar [src='../images/birds_icon.gif']");
    private final SelenideElement backLink = $("#BackLink");
    private final SelenideElement pageTitle = $("#Catalog h2");
    private final ElementsCollection categoryTable = $$("tr");
    private final SelenideElement angelfishSubcategoryLink = $("[href='/actions/Catalog.action?viewProduct=&productId=FI-SW-01']");
    private final SelenideElement tigerSharkSubcategoryLink = $("[href='/actions/Catalog.action?viewProduct=&productId=FI-SW-02']");
    private final SelenideElement koiSubcategoryLink = $("[href='/actions/Catalog.action?viewProduct=&productId=FI-FW-01']");
    private final SelenideElement labradorSubcategoryLink = $("[href='/actions/Catalog.action?viewProduct=&productId=K9-RT-02']");

    @Step("Open Catalog page")
    public void openCatalogPage() {
        open(catalogPageURL);
    }

    public boolean isHeaderDisplayed() {
        return header.isDisplayed();
    }


    public boolean isFooterDisplayed() {
        return footer.isDisplayed();
    }

    public boolean isFishButtonDisplayed() {
        return fishButton.isDisplayed();
    }

    public boolean isDogsButtonDisplayed() {
        return dogsButton.isDisplayed();
    }

    public boolean isCatsButtonDisplayed() {
        return catsButton.isDisplayed();
    }

    public boolean isReptilesButtonDisplayed() {
        return reptilesButton.isDisplayed();
    }

    public boolean isBirdsButtonDisplayed() {
        return birdsButton.isDisplayed();
    }

    @Step("Click on Fish button in Sidebar")
    public void clickOnFishButton() {
        fishButton.click();
    }

    @Step("Click on Dogs button in Sidebar")
    public void clickOnDogsButton() {
        dogsButton.click();
    }

    @Step("Click on Cats button in Sidebar")
    public void clickOnCatsButton() {
        catsButton.click();
    }

    @Step("Click on Reptiles button in Sidebar")
    public void clickOnReptilesButton() {
        reptilesButton.click();
    }

    @Step("Click on Birds button in Sidebar")
    public void clickOnBirdsButton() {
        birdsButton.click();
    }

    public boolean backLinkIsDisplayed() {
        return backLink.isDisplayed();
    }

    @Step("Click on Backlink")
    public void clickOnBackLink() {
        backLink.click();
    }

    public String getPageTitle() {
        return pageTitle.text();
    }

    public int getSubcategoriesNumber() {
        return categoryTable.size() - 1;
    }

    @Step("Click on Angelfish Subcategory")
    public void clickOnAngelfishSubcategoryLink() {
        angelfishSubcategoryLink.click();
    }

    @Step("Click on Koi Subcategory")
    public void clickOnKoiSubcategoryLink() {
        koiSubcategoryLink.click();
    }

    @Step("Click on Labrador Subcategory")
    public void clickOnLabradorSubcategoryLink() {
        labradorSubcategoryLink.click();
    }

    @Step("Click on Tiger Shark Subcategory")
    public void clickOnTigerSharkSubcategoryLink() {
        tigerSharkSubcategoryLink.click();
    }

    public int getItemsTableSize() {
        return categoryTable.size();
    }

    public int getItemsNumber() {
        return categoryTable.size() - 2;
    }

    public String getBackLink() {
        return backLink.text();
    }

    public boolean isMainCatalogDisplayed() {
        return mainCatalog.isDisplayed();
    }

    public String getWelcomeMessage() {
        return welcomeContent.text();
    }
}
