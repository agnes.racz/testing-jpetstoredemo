package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.Keys;

import static com.codeborne.selenide.Selenide.$;

public class CartItem {
    private final SelenideElement cartItemIDLink;
    private final String cartItemProductID;
    private final String cartItemDescription;
    private final String cartItemInStock;
    private final SelenideElement cartItemQuantity;
    private final SelenideElement cartItemListPrice;
    private SelenideElement cartItemTotalCost;
    private final SelenideElement cartItemRemoveButton;

    public CartItem(String cartItemID) {
        String cartItemIDSelector = String.format("[href='/actions/Catalog.action?viewItem=&itemId=%s']", cartItemID);
        this.cartItemIDLink = $(cartItemIDSelector);
        this.cartItemProductID = cartItemIDLink.parent().sibling(0).text();
        this.cartItemDescription = cartItemIDLink.parent().sibling(1).text();
        this.cartItemInStock = cartItemIDLink.parent().sibling(2).text();
        this.cartItemQuantity = cartItemIDLink.parent().sibling(3).$("input");
        this.cartItemListPrice = cartItemIDLink.parent().sibling(4);
        this.cartItemTotalCost = cartItemIDLink.parent().sibling(5);
        this.cartItemRemoveButton = cartItemIDLink.parent().sibling(6).$(".Button");
    }

    public String getCartItemQuantity() {
        return cartItemQuantity.getAttribute("value");
    }

    public String getCartItemID() {
        return cartItemIDLink.text();
    }

    @Step("Click on Remove button")
    public void clickOnRemoveButton() {
        cartItemRemoveButton.click();
    }

    @Step("Change quantity")
    public void changeQuantityInCart(String qty) {
        cartItemQuantity.click();
        cartItemQuantity.clear();
        cartItemQuantity.type(qty);
    }

    public double getTotalCost() {
        String priceText = cartItemTotalCost.text().substring(1);
        return Double.parseDouble(priceText);
    }

}
