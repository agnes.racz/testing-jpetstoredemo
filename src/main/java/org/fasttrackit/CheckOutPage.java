package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;

public class CheckOutPage {
    private final SelenideElement checkOutForm = $("[action='/actions/Order.action']");
    private final SelenideElement continueButton = $("[value='Continue']");
    private final SelenideElement confirmButton = $(".Button");
    private final SelenideElement confirmationMessage = $(".messages");

    public boolean isCheckOutFormDisplayed() {
        return checkOutForm.isDisplayed();
    }

    @Step("Click on Continue button")
    public void clickOnContinueButton() {
        continueButton.click();
    }

    public boolean isConfirmButtonDisplayed() {
        return confirmButton.isDisplayed();
    }

    @Step("Click on Confirm button")
    public void clickOnConfirmButton() {
        confirmButton.click();
    }

    public String getConfirmationMessage(){
        return confirmationMessage.text();
    }

}
