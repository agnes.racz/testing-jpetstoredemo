package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;

public class Header {

    private final SelenideElement logo = $("#Logo");
    private final SelenideElement cartIcon = $("[name='img_cart']");
    private final SelenideElement signInLink = $("#MenuContent a:nth-of-type(2)");
    private final SelenideElement signOutLink = $("[href='/actions/Account.action?signoff=']");
    private final SelenideElement myAccountLink = $("[href='/actions/Account.action?editAccountForm=']");
    private final SelenideElement questionMark = $("[href='../help.html']");
    private final SelenideElement searchBar = $("#SearchContent [type='text']");
    private final SelenideElement searchButton = $("#SearchContent [type='submit']");
    private final SelenideElement quicklinks = $("#QuickLinks");
    private final SelenideElement fishButton = $("#QuickLinks [src='../images/sm_fish.gif']");
    private final SelenideElement dogsButton = $("#QuickLinks [src='../images/sm_dogs.gif']");
    private final SelenideElement reptilesButton = $("#QuickLinks [src='../images/sm_reptiles.gif']");
    private final SelenideElement catsButton = $("#QuickLinks [src='../images/sm_cats.gif']");
    private final SelenideElement birdsButton = $("#QuickLinks [src='../images/sm_birds.gif']");


    public boolean logoIsDisplayed() {
        return logo.isDisplayed();
    }

    public boolean cartIconIsDisplayed() {
        return cartIcon.isDisplayed();
    }

    public boolean signInLinkIsDisplayed() {
        return signInLink.isDisplayed();
    }

    public boolean questionMarkIsDisplayed() {
        return questionMark.isDisplayed();
    }

    public boolean searchBarIsDisplayed() {
        return searchBar.isDisplayed();
    }

    public boolean searchButtonIsDisplayed() {
        return searchButton.isDisplayed();
    }

    public boolean quicklinksIsDisplayed() {
        return quicklinks.isDisplayed();
    }

    public boolean fishButtonIsDisplayed() {
        return fishButton.isDisplayed();
    }

    public boolean dogsButtonIsDisplayed() {
        return dogsButton.isDisplayed();
    }

    public boolean reptilesButtonIsDisplayed() {
        return reptilesButton.isDisplayed();
    }

    public boolean catsButtonIsDisplayed() {
        return catsButton.isDisplayed();
    }

    public boolean birdsButtonIsDisplayed() {
        return birdsButton.isDisplayed();
    }

    @Step("Click on Cart icon")
    public void clickOnCartIcon() {
        cartIcon.click();
    }

    @Step("Click on Sign in Link")
    public void clickOnSignInLink() {
        signInLink.click();
    }

    @Step("Click on Question Mark")
    public void clickOnQuestionMark() {
        questionMark.click();
    }

    @Step("Click on Fish button in Header")
    public void clickOnFishButton() {
        fishButton.click();
    }

    @Step("Click on Dogs button in Header")
    public void clickOnDogsButton() {
        dogsButton.click();
    }

    @Step("Click on Reptiles button in Header")
    public void clickOnReptilesButton() {
        reptilesButton.click();
    }

    @Step("Click on Cats button in Header")
    public void clickOnCatsButton() {
        catsButton.click();
    }

    @Step("Click on Birds button in Header")
    public void clickOnBirdsButton() {
        birdsButton.click();
    }

    public boolean isSignOutLinkDisplayed() {
        return signOutLink.isDisplayed();
    }

    public boolean isMyAccountLinkDisplayed() {
        return myAccountLink.isDisplayed();
    }

    @Step("Type in search word in Search bar")
    public void typeInSearchWord(String word) {
        searchBar.click();
        searchBar.type(word);
    }

    @Step("Click on Search button")
    public void clickOnSearchButton() {
        searchButton.click();
    }
    @Step("Click on Logo")
    public void clickOnLogo(){
        logo.click();
    }
}
