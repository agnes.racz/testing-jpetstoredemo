package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;

public class SignInPage {
    private final SelenideElement errorMessage = $(".messages");
    private final SelenideElement signInForm = $("#Content form");
    private final SelenideElement usernameField = $("[name=username]");
    private final SelenideElement passwordField = $("[name=password]");
    private final SelenideElement loginButton = $("[name=signon]");


    public boolean signInFormIsDisplayed() {
        return signInForm.isDisplayed();
    }

    public String getUsernameInputText() {
        return usernameField.getAttribute("value");
    }

    public String getPasswordInputText() {
        return passwordField.getAttribute("value");
    }

    @Step("Type in Username")
    public void typeInUsername(String newUsername) {
        usernameField.type(newUsername);
    }

    @Step("Type in Password")
    public void typeInPassword(String newPassword) {
        passwordField.clear();
        passwordField.type(newPassword);
    }

    @Step("Click on Login button")
    public void clickOnLoginButton() {
        loginButton.click();
    }

    public void pressEnterOnLoginButton() {
        loginButton.pressEnter();
    }

    public boolean isUsernameFieldDisplayed() {
        return usernameField.isDisplayed();
    }

    public boolean isPasswordFieldDisplayed() {
        return passwordField.isDisplayed();
    }

    public boolean isLoginButtonDisplayed() {
        return loginButton.isDisplayed();
    }

}
