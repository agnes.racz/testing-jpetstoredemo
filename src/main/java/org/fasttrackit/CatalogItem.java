package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;

public class CatalogItem {
    private final SelenideElement itemLink;
    private final SelenideElement productID;
    private final SelenideElement itemDescription;
    private final SelenideElement itemPrice;
    private final SelenideElement itemAddToCartButton;

    public CatalogItem(String itemID) {
        String itemIDSelector = String.format("[href='/actions/Catalog.action?viewItem=&itemId=%s']", itemID);
        this.itemLink = $(itemIDSelector);
        this.productID = itemLink.parent().sibling(0);
        this.itemDescription = itemLink.parent().sibling(1);
        this.itemPrice = itemLink.parent().sibling(2);
        String addToCartButtonSelector = String.format("[href='/actions/Cart.action?addItemToCart=&workingItemId=%s']", itemID);
        this.itemAddToCartButton = $(addToCartButtonSelector);
    }

    public boolean isProductIDDisplayed() {
        return productID.isDisplayed();
    }

    public boolean isItemIDDisplayed() {
        return itemLink.isDisplayed();
    }

    public String getProductID() {
        return productID.text();
    }

    public boolean isDescriptionDisplayed() {
        return itemDescription.isDisplayed();
    }

    public boolean isPriceDisplayed() {
        return itemPrice.isDisplayed();
    }

    public boolean isAddToCartButtonDisplayed() {
        return itemAddToCartButton.isDisplayed();
    }

    @Step("Click on Item ID")
    public void clickOnItemID() {
        itemLink.click();
    }

    @Step("Click on Add to Cart button")
    public void clickOnAddToCartButton() {
        itemAddToCartButton.click();
    }

    public double getPrice() {
        String priceText = itemPrice.text().substring(1);
        return Double.parseDouble(priceText);
    }
}
