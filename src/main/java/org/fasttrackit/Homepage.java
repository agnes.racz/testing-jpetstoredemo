package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class Homepage {
    private final String homepageURL = "https://petstore.octoperf.com/";
    private final SelenideElement welcomeText = $("#Content h2");
    private final SelenideElement enterTheStoreLink = $("[href='actions/Catalog.action']");
    private final SelenideElement copyrightDetails = $("#Content sub");

    @Step("Open Homepage")
    public void openHomepage() {
        open(homepageURL);
    }

    public String getWelcomeText() {
        return welcomeText.text();
    }

    @Step("Click on Enter The Store link")
    public void clickOnEnterTheStoreLink() {
        enterTheStoreLink.click();
    }

    public boolean enterTheStoreLinkDisplayed() {
        return enterTheStoreLink.isDisplayed();
    }

    public boolean copyrightDetailsAreDisplayed() {
        return copyrightDetails.isDisplayed();
    }
}
