# Testing JPetStoreDemo

### Author: Agnes Racz
### Mentor: Rami Sharaiyri

### Description: 
In this application, the JPetStore website is tested: https://petstore.octoperf.com/. 
This is a fictional online commerce web application that sells pets.

The test application is written in Java programming language and various dependencies and plugins are used for automated testing process and report generation.

POM is used for the structure, so that the page objects are separate classes in the application.
Selenide and TestNG dependencies are used for testing. Page elements are defined with CSS Selectors. To expand test coverage, several Data Provider classes are implemented.

The project contains three categories of tests:
- static tests, which check whether elements are displayed on:
    - Footer
    - Header 
    - Catalog page 
    - Home page  


- functional tests, which check the functionality of the:
    - Cart 
    - Login 
    - Search
    - Check out
  

- tests, which check the user's navigation ability on the page

Some tests are run with logged in user, but also with guest user.
The tests have different priorities, which are grouped as follows:
 - **Blocker:** those tests that prevent the user from completing a shopping flow, e.g. the test that checks whether access can be made in the online store or the tests that check whether the user can add a product to the cart.
 - **Critical:** these are the tests that are directly related to a shopping flow, e.g. tests that check if the user can navigate to product subcategories
 - **Normal:** tests that do not prevent the user from making purchases, but are important in the user's decision, e.g. tests that check if some product details are displayed
 - **Minor:** are user interface and user experience tests, e.g. the test that checks the display of the welcome message.

For the tests to run in the background, the Selenide .headless configuration is used. This is making the tests run faster and more efficient.

Generating the report after running all the tests was done by implementing the Allure Reports plugin.

In total, the application runs 248 tests, of which 243 are successful and 5 report a defect each.

![ALT](allure_report_project_desc.jpg)

The next screenshot shows the way the tests cases were organized by test scenarios. TestNG annotations @ Feature and @ Story were introduced for this purpose.
Test steps are also visible on the right side of the screenshot, featuring Setup and Tear Down, introduced with TestNG annotations @BeforeMethod and @AfterMethod.

![ALT](allure_report_-_test_detail.jpg)


### Technologies used:
- Java 17.0.8
- Maven 4.0.0
- Selenide 7.0.2
- Allure 2.24.0
- TestNG 7.8.0